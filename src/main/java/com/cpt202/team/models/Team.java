package com.cpt202.team.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity //database class
//create team table
public class Team {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO) // database generate id 
    private int id;
    private String name;
    private int memberCount; //This will be converted into member_Count by naming strategies in properties
    
    //command+. for constructor
    //empty input
    public Team() {
    }
    
    public Team(int id, String name, int memberCount) {
        this.id = id; 
        this.name = name;
        this.memberCount = memberCount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    //press command+. for getter and setter
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getMemberCount() {
        return memberCount;
    }
    public void setMemberCount(int memberCount) {
        this.memberCount = memberCount;
    }

    

    
}
