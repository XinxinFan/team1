package com.cpt202.team.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cpt202.team.models.Team;

//spring boot will build repository automatically
public interface TeamRepo extends JpaRepository<Team, Integer>{//object, data type of our id
    
}
