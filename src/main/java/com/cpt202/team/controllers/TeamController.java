package com.cpt202.team.controllers;

import java.util.ArrayList;
import java.util.List;
import com.cpt202.team.models.Team;
import com.cpt202.team.repositories.TeamRepo;
import com.cpt202.team.services.TeamService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;

///

@Controller 
@RequestMapping("/team") // if all request divert from /team, then this can be used
public class TeamController {

    @Autowired
    private TeamService teamService;

    // localhost:8080/team/list
    @GetMapping("/list") 
    public String getList(Model model){
        model.addAttribute("teamList", teamService.getTeamList()); 
        // attribute key is teamlist
        return "allTeams";
    }


    //response to get 
    @GetMapping("/add")
    public String addTeam(Model model){ 
        model.addAttribute("team", new Team()); //send an empty Team object
        return "addTeam";
    } 
    
    @PostMapping("/add")
    //bind the model key value "team in addTeam"
    public String confirmNewTeam(@ModelAttribute("team") Team team){
        //add new team into database
        teamService.newTeam(team);
        return "home";
    } 
}
