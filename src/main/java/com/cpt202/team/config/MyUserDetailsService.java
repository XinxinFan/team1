package com.cpt202.team.config;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.cpt202.team.models.User;
import com.cpt202.team.repositories.UserRepo;
import com.cpt202.team.security.MyUserDetails;

@Service
public class MyUserDetailsService implements UserDetailsService{
    //dependency injection
    @Autowired
    private UserRepo userRepo;

    /*
     * retrieve user information from the data source
     * @param username provided during the login process
     * return user detail is username correct
     */
    @Override
    public UserDetails loadUserByUsername(String useranme) throws UsernameNotFoundException {
        //load user from JPA

        //return new MyUserDetails(s);

        Optional<User> ReturnedUser = userRepo.findByUserName(useranme); //retrived user object with given name
        User user = ReturnedUser.orElseThrow(() -> new UsernameNotFoundException("User not found"));

        return new MyUserDetails(user);
    }
    
}
