package com.cpt202.team.config;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfiguration;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
// cross line means newer version is available
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
    /*
     * set up user authentication
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
            .withUser("me")
            .password("pass")
            .roles("USER")
            .and() // add another user
            .withUser("admin")
            .password("pass")
            .roles("ADMIN");
        
    }
    
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()// tell which url is allowing and by who
        .antMatchers("team/add").hasRole("ADMIN")
        .antMatchers("team/list").hasRole("USER")
        .antMatchers("/").permitAll() //should be bottom. 
        //if at first, http will permitAll to access all folders and info
        .and() // start new object
        .formLogin(); // the login form
    }

    @Bean
    public PasswordEncoder getPasswordEncoder(){
        return NoOpPasswordEncoder.getInstance();// no encrytion of password
        //return new BCryptPasswordEncoder(); // the password pass in must be encryted to match
    }
    
}
